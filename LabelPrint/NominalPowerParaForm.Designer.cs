﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/16/2016
 * Time: 8:50 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace LabelPrint
{
	partial class NominalPowerParaForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NominalPowerParaForm));
			this.LBModel = new System.Windows.Forms.ListBox();
			this.BtAdd = new System.Windows.Forms.Button();
			this.BtEdit = new System.Windows.Forms.Button();
			this.BtDelete = new System.Windows.Forms.Button();
			this.BtQuit = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// LBModel
			// 
			this.LBModel.FormattingEnabled = true;
			this.LBModel.Location = new System.Drawing.Point(26, 54);
			this.LBModel.Name = "LBModel";
			this.LBModel.Size = new System.Drawing.Size(205, 290);
			this.LBModel.TabIndex = 0;
			// 
			// BtAdd
			// 
			this.BtAdd.Location = new System.Drawing.Point(252, 74);
			this.BtAdd.Name = "BtAdd";
			this.BtAdd.Size = new System.Drawing.Size(73, 32);
			this.BtAdd.TabIndex = 1;
			this.BtAdd.Text = "Add";
			this.BtAdd.UseVisualStyleBackColor = true;
			this.BtAdd.Click += new System.EventHandler(this.BtAddClick);
			// 
			// BtEdit
			// 
			this.BtEdit.Location = new System.Drawing.Point(252, 135);
			this.BtEdit.Name = "BtEdit";
			this.BtEdit.Size = new System.Drawing.Size(73, 32);
			this.BtEdit.TabIndex = 1;
			this.BtEdit.Text = "Edit";
			this.BtEdit.UseVisualStyleBackColor = true;
			this.BtEdit.Click += new System.EventHandler(this.BtEditClick);
			// 
			// BtDelete
			// 
			this.BtDelete.Location = new System.Drawing.Point(252, 189);
			this.BtDelete.Name = "BtDelete";
			this.BtDelete.Size = new System.Drawing.Size(73, 32);
			this.BtDelete.TabIndex = 2;
			this.BtDelete.Text = "Delete";
			this.BtDelete.UseVisualStyleBackColor = true;
			this.BtDelete.Click += new System.EventHandler(this.BtDeleteClick);
			// 
			// BtQuit
			// 
			this.BtQuit.Location = new System.Drawing.Point(252, 312);
			this.BtQuit.Name = "BtQuit";
			this.BtQuit.Size = new System.Drawing.Size(73, 32);
			this.BtQuit.TabIndex = 3;
			this.BtQuit.Text = "Quit";
			this.BtQuit.UseVisualStyleBackColor = true;
			this.BtQuit.Click += new System.EventHandler(this.BtQuitClick);
			// 
			// NominalPowerParaForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(352, 392);
			this.Controls.Add(this.BtQuit);
			this.Controls.Add(this.BtDelete);
			this.Controls.Add(this.BtEdit);
			this.Controls.Add(this.BtAdd);
			this.Controls.Add(this.LBModel);
			this.Name = "NominalPowerParaForm";
			this.Text = "NominalPowerParaForm";
			this.ResumeLayout(false);

		}
		private System.Windows.Forms.Button BtQuit;
		private System.Windows.Forms.Button BtDelete;
		private System.Windows.Forms.Button BtEdit;
		private System.Windows.Forms.Button BtAdd;
		private System.Windows.Forms.ListBox LBModel;
	}
}
