﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/16/2016
 * Time: 2:36 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace LabelPrint
{
	partial class SchemeManageForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SchemeManageForm));
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.SchemeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SchemeNM = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.BtQuit = new System.Windows.Forms.Button();
			this.BtAdd = new System.Windows.Forms.Button();
			this.BtEdit = new System.Windows.Forms.Button();
			this.BtDelete = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.AllowUserToResizeColumns = false;
			this.dataGridView1.AllowUserToResizeRows = false;
			this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
			this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.SchemeID,
			this.SchemeNM});
			this.dataGridView1.GridColor = System.Drawing.Color.White;
			this.dataGridView1.Location = new System.Drawing.Point(23, 60);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ReadOnly = true;
			this.dataGridView1.RowHeadersVisible = false;
			this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridView1.Size = new System.Drawing.Size(304, 333);
			this.dataGridView1.TabIndex = 0;
			// 
			// SchemeID
			// 
			this.SchemeID.HeaderText = "SchemeID";
			this.SchemeID.Name = "SchemeID";
			this.SchemeID.ReadOnly = true;
			this.SchemeID.Width = 150;
			// 
			// SchemeNM
			// 
			this.SchemeNM.HeaderText = "SchemeNM";
			this.SchemeNM.Name = "SchemeNM";
			this.SchemeNM.ReadOnly = true;
			this.SchemeNM.Width = 150;
			// 
			// BtQuit
			// 
			this.BtQuit.Location = new System.Drawing.Point(353, 365);
			this.BtQuit.Name = "BtQuit";
			this.BtQuit.Size = new System.Drawing.Size(64, 28);
			this.BtQuit.TabIndex = 1;
			this.BtQuit.Text = "Quit";
			this.BtQuit.UseVisualStyleBackColor = true;
			this.BtQuit.Click += new System.EventHandler(this.BtQuitClick);
			// 
			// BtAdd
			// 
			this.BtAdd.Location = new System.Drawing.Point(352, 94);
			this.BtAdd.Name = "BtAdd";
			this.BtAdd.Size = new System.Drawing.Size(76, 32);
			this.BtAdd.TabIndex = 2;
			this.BtAdd.Text = "Add";
			this.BtAdd.UseVisualStyleBackColor = true;
			this.BtAdd.Click += new System.EventHandler(this.BtAddClick);
			// 
			// BtEdit
			// 
			this.BtEdit.Location = new System.Drawing.Point(353, 157);
			this.BtEdit.Name = "BtEdit";
			this.BtEdit.Size = new System.Drawing.Size(76, 32);
			this.BtEdit.TabIndex = 3;
			this.BtEdit.Text = "Edit";
			this.BtEdit.UseVisualStyleBackColor = true;
			this.BtEdit.Click += new System.EventHandler(this.BtEditClick);
			// 
			// BtDelete
			// 
			this.BtDelete.Location = new System.Drawing.Point(353, 216);
			this.BtDelete.Name = "BtDelete";
			this.BtDelete.Size = new System.Drawing.Size(76, 32);
			this.BtDelete.TabIndex = 4;
			this.BtDelete.Text = "Delete";
			this.BtDelete.UseVisualStyleBackColor = true;
			this.BtDelete.Click += new System.EventHandler(this.BtDeleteClick);
			// 
			// SchemeManageForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(440, 433);
			this.Controls.Add(this.BtDelete);
			this.Controls.Add(this.BtEdit);
			this.Controls.Add(this.BtAdd);
			this.Controls.Add(this.BtQuit);
			this.Controls.Add(this.dataGridView1);
			this.Name = "SchemeManageForm";
			this.Text = "SchemeManageForm";
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);

		}
		private System.Windows.Forms.Button BtDelete;
		private System.Windows.Forms.Button BtEdit;
		private System.Windows.Forms.Button BtAdd;
		private System.Windows.Forms.Button BtQuit;
		private System.Windows.Forms.DataGridViewTextBoxColumn SchemeNM;
		private System.Windows.Forms.DataGridViewTextBoxColumn SchemeID;
		private System.Windows.Forms.DataGridView dataGridView1;
	}
}
