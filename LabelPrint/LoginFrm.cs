﻿/*
 * Created by SharpDevelop.
 * User: Zach.Read
 * Date: 18-06-15
 * Time: 14:03
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace LabelPrint
{
	/// <summary>
	/// Description of LoginFrm.
	/// </summary>
	public partial class LoginFrm : Form
	{
		public static string usernm = "";
		public static int userID = -1;
		public LoginFrm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			textBox1.Select();
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		
		
		void LoginClick(object sender, EventArgs e)
		{
			string user,pwd;
            user = textBox1.Text.Trim();
            if (user.Length == 0)
            {
                MessageBox.Show("Username can not be empty");
                return;
            }
            
            pwd = textBox2.Text.Trim();
            if (pwd.Length == 0)
            {
                MessageBox.Show("Password can not be empty");
                return;
            }


            if (verifyUser(user, pwd))
            {
                usernm = user;
				string sql =@" select top 1 interid from UserLogin where UserNM ='"+ user+"' ";
				SqlDataReader rd = ToolsClass.GetDataReader(sql);
				if(rd.Read() && rd != null)
					userID = Convert.ToInt32(rd.GetValue(0));
            	MainForm frm = new MainForm();
				this.Hide();
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();				
				this.Close();	
            }
            else
            {
                MessageBox.Show("Username and password is incorrect or you do not have sufficient permissions.");
            }	
		}
		
		private bool verifyUser(string username, string password)
		{
			
			//String sqlCon = @"Data Source=CA01s0015; Initial Catalog=LabelPrintDB; User ID=Fastengine; Password=Csi456";
			string sqlCon = ToolsClass.getConfig ("Conn",false,"","Config.xml");	
			string sqlCmd = @"SELECT [UserGroup] FROM [UserLogin] WHERE [UserNM]='" + username + "' AND [UserPW]='" + password + "'";
			string result = "";
			
			using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(sqlCon))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                try
                {
                    conn.Open();                  
                    cmd.CommandText = sqlCmd;
                    System.Data.SqlClient.SqlDataReader read = cmd.ExecuteReader();
                    if (read.Read())
                    {                    	
                    	result = (String.Format("{0}", read[0]));
                    	if(result == "9" || result == "10" || username == "user03")
                    	{
                    		return true;
                    	}
                    	else{
                    		return false;
                    	}                    	
                    }
					return false;                    
                }
                catch{
                	MessageBox.Show("Fail");
                	return false;
                }
			}
		}
		
		void textBox1_KeyPress(object sender, KeyPressEventArgs e )
		{
			if(e.KeyChar == '\r')
			{
				textBox2.Focus();
				textBox2.Select();
			}
		}
		void textBox2_KeyPress(object sender, KeyPressEventArgs e )
		{
			if(e.KeyChar == '\r')
			{
				Login.Focus();
				LoginClick(sender,e);
			}
			
		}
		
	}
}
