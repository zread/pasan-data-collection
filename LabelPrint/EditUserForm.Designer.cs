﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 2/2/2016
 * Time: 3:45 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace LabelPrint
{
	partial class EditUserForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditUserForm));
			this.label1 = new System.Windows.Forms.Label();
			this.Tb_UN = new System.Windows.Forms.TextBox();
			this.Tb_SP = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.Tb_PW = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.Tb_CFPW = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.Bt_Ok = new System.Windows.Forms.Button();
			this.Bt_Cancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(40, 78);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(71, 18);
			this.label1.TabIndex = 0;
			this.label1.Text = "User Name:";
			// 
			// Tb_UN
			// 
			this.Tb_UN.Location = new System.Drawing.Point(117, 75);
			this.Tb_UN.Name = "Tb_UN";
			this.Tb_UN.ReadOnly = true;
			this.Tb_UN.Size = new System.Drawing.Size(157, 20);
			this.Tb_UN.TabIndex = 1;
			// 
			// Tb_SP
			// 
			this.Tb_SP.Location = new System.Drawing.Point(117, 102);
			this.Tb_SP.Multiline = true;
			this.Tb_SP.Name = "Tb_SP";
			this.Tb_SP.Size = new System.Drawing.Size(157, 18);
			this.Tb_SP.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(40, 102);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(71, 18);
			this.label2.TabIndex = 2;
			this.label2.Text = "Specification";
			// 
			// Tb_PW
			// 
			this.Tb_PW.Location = new System.Drawing.Point(117, 126);
			this.Tb_PW.Name = "Tb_PW";
			this.Tb_PW.Size = new System.Drawing.Size(157, 20);
			this.Tb_PW.TabIndex = 5;
			this.Tb_PW.UseSystemPasswordChar = true;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(40, 129);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(71, 18);
			this.label3.TabIndex = 4;
			this.label3.Text = "Password:";
			// 
			// Tb_CFPW
			// 
			this.Tb_CFPW.Location = new System.Drawing.Point(117, 155);
			this.Tb_CFPW.Name = "Tb_CFPW";
			this.Tb_CFPW.Size = new System.Drawing.Size(157, 20);
			this.Tb_CFPW.TabIndex = 7;
			this.Tb_CFPW.UseSystemPasswordChar = true;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(40, 158);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(71, 18);
			this.label4.TabIndex = 6;
			this.label4.Text = "Confirm PW:";
			// 
			// Bt_Ok
			// 
			this.Bt_Ok.Location = new System.Drawing.Point(79, 191);
			this.Bt_Ok.Name = "Bt_Ok";
			this.Bt_Ok.Size = new System.Drawing.Size(71, 28);
			this.Bt_Ok.TabIndex = 8;
			this.Bt_Ok.Text = "OK";
			this.Bt_Ok.UseVisualStyleBackColor = true;
			this.Bt_Ok.Click += new System.EventHandler(this.Bt_OkClick);
			// 
			// Bt_Cancel
			// 
			this.Bt_Cancel.Location = new System.Drawing.Point(208, 191);
			this.Bt_Cancel.Name = "Bt_Cancel";
			this.Bt_Cancel.Size = new System.Drawing.Size(77, 28);
			this.Bt_Cancel.TabIndex = 9;
			this.Bt_Cancel.Text = "Cancel";
			this.Bt_Cancel.UseVisualStyleBackColor = true;
			this.Bt_Cancel.Click += new System.EventHandler(this.Bt_CancelClick);
			// 
			// EditUserForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(333, 246);
			this.Controls.Add(this.Bt_Cancel);
			this.Controls.Add(this.Bt_Ok);
			this.Controls.Add(this.Tb_CFPW);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.Tb_PW);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.Tb_SP);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.Tb_UN);
			this.Controls.Add(this.label1);
			this.Name = "EditUserForm";
			this.Text = "EditUserForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.Button Bt_Cancel;
		private System.Windows.Forms.Button Bt_Ok;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox Tb_CFPW;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox Tb_PW;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox Tb_SP;
		private System.Windows.Forms.TextBox Tb_UN;
		private System.Windows.Forms.Label label1;
	}
}
