﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 2/2/2016
 * Time: 10:33 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace LabelPrint
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
	
		
		
		
		#region Populate			
		public void AutoPopulate(){
			
		
			listBox2.Items.Clear();//Clear all Data from Sepecific Defect.
			string Function = listBox1.SelectedItem.ToString();
			if( Function=="1.User Management"){
				listBox2.Items.AddRange(new object[] {
				                         	"1.1 User Maintain",											
				                   });
			}
			else if( Function=="2.Master Data Management"){
				listBox2.Items.AddRange(new object[] {
				                         	"2.1 Master Data Setting",											
				                   });
			}
			else if( Function=="3.Nominal Power Management"){
				listBox2.Items.AddRange(new object[] {
				                         	"3.1 Nominal Power Class Setting",
											"3.2 Nomial Power Parameter Setting"				                         	
				                   });
			}
			else if( Function=="4.Decimal Place Management"){
				listBox2.Items.AddRange(new object[] {
				                         	"4.1 Measured Data Decimal Place Setting"														                         	
				                   });
			}
			else if( Function=="5.Scheme Management"){
				listBox2.Items.AddRange(new object[] {
				                         	"5.1 Scheme Configuration"														                         	
				                   });
			}
			else if( Function=="6.Scheme Executing"){
				listBox2.Items.AddRange(new object[] {
				                         	"6.1 Scheme Executing"														                         	
				                   });
			}
																											
}
		void ListBox1SelectedIndexChanged(object sender, EventArgs e)
		{
			AutoPopulate();
		}
		#endregion
		private void Listbox2_doubleclick(object sender, EventArgs e)
		{
			string temp = "";
			if(listBox2.SelectedIndex > -1)
			 temp = listBox2.SelectedItem.ToString();
			
			if(temp == "1.1 User Maintain" )
			{
				UserForm frm = new UserForm();				
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();
			}		
			else if(temp == "2.1 Master Data Setting" )
			{
				MasterDataForm frm = new MasterDataForm();
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();
			}			
			else if(temp == "3.1 Nominal Power Class Setting" )
			{
				NominalPowerClssForm frm = new NominalPowerClssForm();
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();				
			}
			else if(temp == "3.2 Nomial Power Parameter Setting" )
			{
				NominalPowerParaForm frm = new NominalPowerParaForm();
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();
			}
			else if(temp == "4.1 Measured Data Decimal Place Setting" )
			{
				DecimalDigitsFrom frm = new DecimalDigitsFrom();
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();
			}
			else if(temp == "5.1 Scheme Configuration" )
			{
				SchemeManageForm frm = new SchemeManageForm();
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();
			}
			else if(temp == "6.1 Scheme Executing" )
			{
				
				SchemeRunForm frm = new SchemeRunForm();
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();
				ToolsClass.WriteToLog("Program Stopped by X");
				
			}
		}
		
		
	}
}
