﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/10/2016
 * Time: 3:55 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace LabelPrint
{
	partial class AddNominalPowerForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddNominalPowerForm));
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.TbCode = new System.Windows.Forms.TextBox();
			this.TbName = new System.Windows.Forms.TextBox();
			this.BtSubmit = new System.Windows.Forms.Button();
			this.BtCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(60, 63);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(53, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Code";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(60, 110);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(52, 20);
			this.label2.TabIndex = 1;
			this.label2.Text = "Name";
			// 
			// TbCode
			// 
			this.TbCode.Location = new System.Drawing.Point(119, 63);
			this.TbCode.Name = "TbCode";
			this.TbCode.Size = new System.Drawing.Size(120, 20);
			this.TbCode.TabIndex = 2;
			// 
			// TbName
			// 
			this.TbName.Location = new System.Drawing.Point(119, 107);
			this.TbName.Name = "TbName";
			this.TbName.Size = new System.Drawing.Size(120, 20);
			this.TbName.TabIndex = 3;
			// 
			// BtSubmit
			// 
			this.BtSubmit.Location = new System.Drawing.Point(55, 172);
			this.BtSubmit.Name = "BtSubmit";
			this.BtSubmit.Size = new System.Drawing.Size(75, 23);
			this.BtSubmit.TabIndex = 4;
			this.BtSubmit.Text = "Submit";
			this.BtSubmit.UseVisualStyleBackColor = true;
			this.BtSubmit.Click += new System.EventHandler(this.BtSubmitClick);
			// 
			// BtCancel
			// 
			this.BtCancel.Location = new System.Drawing.Point(164, 172);
			this.BtCancel.Name = "BtCancel";
			this.BtCancel.Size = new System.Drawing.Size(75, 23);
			this.BtCancel.TabIndex = 6;
			this.BtCancel.Text = "Cancel";
			this.BtCancel.UseVisualStyleBackColor = true;
			this.BtCancel.Click += new System.EventHandler(this.BtCancelClick);
			// 
			// AddNominalPowerForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(303, 225);
			this.Controls.Add(this.BtCancel);
			this.Controls.Add(this.BtSubmit);
			this.Controls.Add(this.TbName);
			this.Controls.Add(this.TbCode);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "AddNominalPowerForm";
			this.Text = "AddNominalPowerForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.Button BtCancel;
		private System.Windows.Forms.Button BtSubmit;
		private System.Windows.Forms.TextBox TbName;
		private System.Windows.Forms.TextBox TbCode;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
	}
}
