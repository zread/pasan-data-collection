﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 2/2/2016
 * Time: 12:01 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace LabelPrint
{
	partial class UserForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserForm));
			this.User = new System.Windows.Forms.Label();
			this.LB_User = new System.Windows.Forms.ListBox();
			this.Bt_Add = new System.Windows.Forms.Button();
			this.Bt_Edit = new System.Windows.Forms.Button();
			this.Bt_Delete = new System.Windows.Forms.Button();
			this.Bt_Exit = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// User
			// 
			this.User.Location = new System.Drawing.Point(32, 49);
			this.User.Name = "User";
			this.User.Size = new System.Drawing.Size(81, 16);
			this.User.TabIndex = 0;
			this.User.Text = "List of Users";
			// 
			// LB_User
			// 
			this.LB_User.FormattingEnabled = true;
			this.LB_User.Location = new System.Drawing.Point(34, 68);
			this.LB_User.Name = "LB_User";
			this.LB_User.ScrollAlwaysVisible = true;
			this.LB_User.Size = new System.Drawing.Size(184, 303);
			this.LB_User.TabIndex = 1;
			// 
			// Bt_Add
			// 
			this.Bt_Add.Location = new System.Drawing.Point(265, 94);
			this.Bt_Add.Name = "Bt_Add";
			this.Bt_Add.Size = new System.Drawing.Size(67, 30);
			this.Bt_Add.TabIndex = 2;
			this.Bt_Add.Text = "Add";
			this.Bt_Add.UseVisualStyleBackColor = true;
			this.Bt_Add.Click += new System.EventHandler(this.Bt_AddClick);
			// 
			// Bt_Edit
			// 
			this.Bt_Edit.Location = new System.Drawing.Point(265, 156);
			this.Bt_Edit.Name = "Bt_Edit";
			this.Bt_Edit.Size = new System.Drawing.Size(67, 30);
			this.Bt_Edit.TabIndex = 3;
			this.Bt_Edit.Text = "Edit";
			this.Bt_Edit.UseVisualStyleBackColor = true;
			this.Bt_Edit.Click += new System.EventHandler(this.Bt_EditClick);
			// 
			// Bt_Delete
			// 
			this.Bt_Delete.Location = new System.Drawing.Point(265, 223);
			this.Bt_Delete.Name = "Bt_Delete";
			this.Bt_Delete.Size = new System.Drawing.Size(67, 30);
			this.Bt_Delete.TabIndex = 4;
			this.Bt_Delete.Text = "Delete";
			this.Bt_Delete.UseVisualStyleBackColor = true;
			this.Bt_Delete.Click += new System.EventHandler(this.Bt_DeleteClick);
			// 
			// Bt_Exit
			// 
			this.Bt_Exit.Location = new System.Drawing.Point(265, 344);
			this.Bt_Exit.Name = "Bt_Exit";
			this.Bt_Exit.Size = new System.Drawing.Size(67, 30);
			this.Bt_Exit.TabIndex = 5;
			this.Bt_Exit.Text = "Exit";
			this.Bt_Exit.UseVisualStyleBackColor = true;
			this.Bt_Exit.Click += new System.EventHandler(this.Bt_ExitClick);
			// 
			// UserForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(380, 413);
			this.Controls.Add(this.Bt_Exit);
			this.Controls.Add(this.Bt_Delete);
			this.Controls.Add(this.Bt_Edit);
			this.Controls.Add(this.Bt_Add);
			this.Controls.Add(this.LB_User);
			this.Controls.Add(this.User);
			this.Name = "UserForm";
			this.Text = "UserForm";
			this.ResumeLayout(false);

		}
		private System.Windows.Forms.Button Bt_Exit;
		private System.Windows.Forms.Button Bt_Delete;
		private System.Windows.Forms.Button Bt_Edit;
		private System.Windows.Forms.Button Bt_Add;
		private System.Windows.Forms.ListBox LB_User;
		private System.Windows.Forms.Label User;
	}
}
