﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 2/16/2016
 * Time: 11:29 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace LabelPrint
{
	partial class AddMasterDataForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddMasterDataForm));
			this.Tb_Name = new System.Windows.Forms.TextBox();
			this.Tb_Code = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.Bt_Sub = new System.Windows.Forms.Button();
			this.Bt_Cancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// Tb_Name
			// 
			this.Tb_Name.Location = new System.Drawing.Point(158, 95);
			this.Tb_Name.Name = "Tb_Name";
			this.Tb_Name.Size = new System.Drawing.Size(118, 20);
			this.Tb_Name.TabIndex = 9;
			// 
			// Tb_Code
			// 
			this.Tb_Code.Location = new System.Drawing.Point(158, 57);
			this.Tb_Code.Name = "Tb_Code";
			this.Tb_Code.ReadOnly = true;
			this.Tb_Code.Size = new System.Drawing.Size(118, 20);
			this.Tb_Code.TabIndex = 8;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(12, 94);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(140, 26);
			this.label2.TabIndex = 7;
			this.label2.Text = "Master Data Name:";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(12, 56);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(140, 26);
			this.label1.TabIndex = 6;
			this.label1.Text = "Master Data Code:";
			// 
			// Bt_Sub
			// 
			this.Bt_Sub.Location = new System.Drawing.Point(28, 158);
			this.Bt_Sub.Name = "Bt_Sub";
			this.Bt_Sub.Size = new System.Drawing.Size(99, 28);
			this.Bt_Sub.TabIndex = 10;
			this.Bt_Sub.Text = "Submit";
			this.Bt_Sub.UseVisualStyleBackColor = true;
			this.Bt_Sub.Click += new System.EventHandler(this.Bt_SubClick);
			// 
			// Bt_Cancel
			// 
			this.Bt_Cancel.Location = new System.Drawing.Point(177, 158);
			this.Bt_Cancel.Name = "Bt_Cancel";
			this.Bt_Cancel.Size = new System.Drawing.Size(99, 28);
			this.Bt_Cancel.TabIndex = 11;
			this.Bt_Cancel.Text = "Cancel";
			this.Bt_Cancel.UseVisualStyleBackColor = true;
			this.Bt_Cancel.Click += new System.EventHandler(this.Bt_CancelClick);
			// 
			// AddMasterDataForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(306, 223);
			this.Controls.Add(this.Bt_Cancel);
			this.Controls.Add(this.Bt_Sub);
			this.Controls.Add(this.Tb_Name);
			this.Controls.Add(this.Tb_Code);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "AddMasterDataForm";
			this.Text = "AddMasterDataForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.Button Bt_Cancel;
		private System.Windows.Forms.Button Bt_Sub;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox Tb_Code;
		private System.Windows.Forms.TextBox Tb_Name;
	}
}
