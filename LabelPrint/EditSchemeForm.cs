﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/16/2016
 * Time: 2:58 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;


namespace LabelPrint
{
	/// <summary>
	/// Description of EditSchemeForm.
	/// </summary>
	public partial class EditSchemeForm : Form
	{
		bool Add;
		int label_Qty = 1;
		int EditInterID = 0;
	
		public EditSchemeForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			ListAllSelection();
			//
			Add = true;
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
			public EditSchemeForm(string sID)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			ListAllSelection();
			//
			TbCode.Enabled = false;
			
			ListAllValues(sID);
			Add = false;
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		private bool VarifyAllEntries()
		{
			bool check = true;
			foreach (Control ctl in Controls) {
				if(string.IsNullOrEmpty(ctl.Text.ToString()))
					check = false;			
			}
			return check;
		}
		
		void ListAllValues(string schemeId)
		{
			TbCode.Text = schemeId;

		//								0			1				2			3				4			5			6			7			8				9			10			11				12				13
			string sql =@"select [SchemeNM] ,[ItemMPID] ,[ItemColorID] ,[ItemSizeID] ,[ItemPointID],[ItemLogoID],[LogoQty],[ItemLableID] ,[LableQty],[ItemModelID] ,[ItemPmaxID],[CreateDate],[ModifyDate],[ItemPatternID],[InterID]					     
					  FROM [LabelPrintDB].[dbo].[Scheme] where SchemeID = '"+schemeId+"'";
			DataTable dt = ToolsClass.PullData(sql);
			TbName.Text = dt.Rows[0][0].ToString();
			EditInterID = Convert.ToInt32(dt.Rows[0][14].ToString());
			SqlDataReader rd = null;
			foreach (Control ctl in Controls) {
				sql = @"select [ItemValue] From Item where interid = '{0}'";
				switch (ctl.Name.ToString()) {
					case "CbModel":
						{
							sql = string.Format(sql,dt.Rows[0][9].ToString());
							rd = ToolsClass.GetDataReader(sql);
							if(rd.Read() && rd != null)
								CbModel.Text = rd.GetString(0);
						}
						break;
					case "CbCellType":
						{
							sql = string.Format(sql,dt.Rows[0][1].ToString());
								rd = ToolsClass.GetDataReader(sql);
							if(rd.Read() && rd != null)
								CbCellType.Text = rd.GetString(0);
						}
						break;
					case "CbLabel":
						{
							sql = string.Format(sql,dt.Rows[0][7].ToString());
								rd = ToolsClass.GetDataReader(sql);
							if(rd.Read() && rd != null)
								CbLabel.Text = rd.GetString(0);
						}
						break;
					case "CbPower":
						{
							sql = string.Format(sql,dt.Rows[0][5].ToString());
							rd = ToolsClass.GetDataReader(sql);
							if(rd.Read() && rd != null)
								CbPower.Text = rd.GetString(0);
						}
						break;
					case "CbColor":
						{
							sql = string.Format(sql,dt.Rows[0][2].ToString());
							rd = ToolsClass.GetDataReader(sql);
							if(rd.Read() && rd != null)
								CbColor.Text =rd.GetString(0);
						}
						break;
					case "CbSize":
						{
							sql = string.Format(sql,dt.Rows[0][3].ToString());
							rd = ToolsClass.GetDataReader(sql);
							if(rd.Read() && rd != null)
								CbSize.Text = rd.GetString(0);
						}
						break;
					case "CbPattern":
						{
							sql = string.Format(sql,dt.Rows[0][13].ToString());
							rd = ToolsClass.GetDataReader(sql);
							if(rd.Read() && rd != null)
								CbPattern.Text = rd.GetString(0);
						}
						break;	
					case "CbDecimal":
						{
							sql = string.Format("select PointID from Point where interid = '{0}'",dt.Rows[0][4].ToString());
							rd = ToolsClass.GetDataReader(sql);
							if(rd.Read() && rd != null)
								CbDecimal.Text = rd.GetString(0);
						}
						break;
					case "CbNominal":
						{
							CbNominal.Text = dt.Rows[0][10].ToString();					
								
						}
						break;	
					
				}
			}
			
			
			
		}
		void ListAllSelection()
		{
			string sql = @"select [ItemClassInterID]
							      ,[ItemID]
							      ,[ItemValue] From Item  order by ItemClassInterID";
			DataTable dt = ToolsClass.PullData(sql);
			foreach (DataRow row in dt.Rows) {
				switch (row[0].ToString()) {
					case "1" :
						CbModel.Items.Add(row[2].ToString());
						break;
					case "3":
						CbCellType.Items.Add(row[2].ToString());
						break;
					case "4":
						CbLabel.Items.Add(row[2].ToString());
						break;
					case "5":
						CbPower.Items.Add(row[2].ToString());
						break;
					case "6":
						CbColor.Items.Add(row[2].ToString());
						break;
					case "7":
						CbSize.Items.Add(row[2].ToString());
						break;
					case "8":
						CbPattern.Items.Add(row[2].ToString());
						break;											
					default:
						break;
				}
			}
			
			sql = @"select distinct PmaxID from SchemeEntry";
			dt.Rows.Clear();
			dt = ToolsClass.PullData(sql);
			foreach (DataRow row in dt.Rows) {
				CbNominal.Items.Add(row[0].ToString());
			}
			
			sql = @"select distinct pointID from Point";
			dt.Rows.Clear();
			dt = ToolsClass.PullData(sql);
			foreach (DataRow row in dt.Rows) {
				CbDecimal.Items.Add(row[0].ToString());
			}
			
			
			
		}
		
		
		
		void BtSubmitClick(object sender, EventArgs e)
		{
			int interid = 0;
			int ModelID = 0, PowerId = 0, ColorID =0, NominalID = 0, PatternID = 0, LabelID = 0, SizeID = 0, CellTypeID = 0, DecimalID = 0;			
			if(!VarifyAllEntries())
				return;
			string sql = @"select top 1 interid from scheme order by interid desc";
			SqlDataReader rd = ToolsClass.GetDataReader(sql);
			if(rd.Read() && rd != null)
				interid = Convert.ToInt32(rd.GetValue(0));
			rd.Close();
			foreach (Control ctl in Controls ) {
				switch (ctl.Name.ToString()) {
					case "CbModel":
						{
							sql = " select top 1 interID from item where ItemClassInterID = 1 and itemvalue = '" + ctl.Text.ToString() +"'";
							rd = ToolsClass.GetDataReader(sql);
							if(rd.Read() && rd != null)
							ModelID = Convert.ToInt32(rd.GetValue(0));
						}
						break;
					case "CbCellType":
						{
							sql = " select top 1 interID from item where ItemClassInterID = 3 and itemvalue = '" + ctl.Text.ToString() +"'";
							rd = ToolsClass.GetDataReader(sql);
							if(rd.Read() && rd != null)
							CellTypeID = Convert.ToInt32(rd.GetValue(0));
						}
						break;
					case "CbLabel":
						{
							sql = " select top 1 interID from item where ItemClassInterID = 4 and itemvalue = '" + ctl.Text.ToString() +"'";
							rd = ToolsClass.GetDataReader(sql);
							if(rd.Read() && rd != null)
							LabelID = Convert.ToInt32(rd.GetValue(0));
						}
						break;
					case "CbPower":
						{
							sql = " select top 1 interID from item where ItemClassInterID = 5 and itemvalue = '" + ctl.Text.ToString() +"'";
							rd = ToolsClass.GetDataReader(sql);
							if(rd.Read() && rd != null)
							PowerId = Convert.ToInt32(rd.GetValue(0));
						}
						break;
					case "CbColor":
						{
							sql = " select top 1 interID from item where ItemClassInterID = 6 and itemvalue = '" + ctl.Text.ToString() +"'";
							rd = ToolsClass.GetDataReader(sql);
							if(rd.Read() && rd != null)
							ColorID = Convert.ToInt32(rd.GetValue(0));
						}
						break;
					case "CbSize":
						{
							sql = " select top 1 interID from item where ItemClassInterID = 7 and itemvalue = '" + ctl.Text.ToString() +"'";
							rd = ToolsClass.GetDataReader(sql);
							if(rd.Read() && rd != null)
							SizeID = Convert.ToInt32(rd.GetValue(0));
						}
						break;
					case "CbPattern":
						{
							sql = " select top 1 interID from item where ItemClassInterID = 8 and itemvalue = '" + ctl.Text.ToString() +"'";
							rd = ToolsClass.GetDataReader(sql);
							if(rd.Read() && rd != null)
							PatternID = Convert.ToInt32(rd.GetValue(0));
						}
						break;	
					case "CbDecimal":
						{
							sql = " select top 1 interID from point where pointID = '" + ctl.Text.ToString() +"'";
							rd = ToolsClass.GetDataReader(sql);
							if(rd.Read() && rd != null)
							DecimalID = Convert.ToInt32(rd.GetValue(0));
						}
						break;	
				
				}
				
				if(rd!= null)
					rd.Close();
			}
			
			if(Add)
			{
				sql = @"select * from scheme where schemeid = '" + TbCode.Text.ToString() + "'";
				DataTable dt = ToolsClass.PullData(sql);
				if(dt.Rows.Count > 0)
				{
					MessageBox.Show("Already exist, please go to edit!");
					this.Close();
					return;
				}
				
				
				sql = @"insert into scheme values({0},'{1}','{2}',{3},{4},{5},{6},{7},{8},{9},{10},{11},'{12}',getdate(),null,{13},null,{14},{15})";
				sql = string.Format(sql, interid +1, TbCode.Text.ToString(),TbName.Text.ToString(),
				                    CellTypeID,ColorID,SizeID,DecimalID,PowerId,label_Qty,LabelID,
				                    label_Qty,ModelID,CbNominal.Text.ToString(),LoginFrm.userID,0,PatternID );
				
				int i = ToolsClass.PutsData(sql);
				if(i != 1)				
					MessageBox.Show("Error, please check!");
				else
				{
					MessageBox.Show("Succesfull!");
					this.Close();
				}
				
				return;
			}			
			else //Edit
			{
				
				sql = @"update scheme set SchemeNM = '{0}',ItemMPID ='{1}', ItemColorID ='{2}',ItemSizeID='{3}',
						ItemPointID = '{4}',ItemLogoID = '{5}',LogoQty = '{6}',ItemLableID = '{7}', LableQty = '{8}',
						ItemModelID = '{9}',ItemPmaxID ='{10}',ModifyDate = getdate(),ModifyUserID ='{11}',ItemPatternID = '{12}' 
						where InterID = '{13}' and SchemeID ='{14}'";
				sql = string.Format(sql,  TbName.Text.ToString(), CellTypeID,ColorID,SizeID,DecimalID,PowerId,label_Qty,LabelID,
				                    label_Qty,ModelID,CbNominal.Text.ToString(),LoginFrm.userID,PatternID,EditInterID, TbCode.Text.ToString());
				
				int i = ToolsClass.PutsData(sql);
				if(i != 1)
				{
					MessageBox.Show("Error, please check!");
					this.Close();
				}
				else
				{
					MessageBox.Show("Succesfull!");
					this.Close();
				}
				return;
			}
		}
		
		void BtCancelClick(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
