﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 2/2/2016
 * Time: 12:01 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;


namespace LabelPrint
{
	/// <summary>
	/// Description of UserForm.
	/// </summary>
	public partial class UserForm : Form
	{
		
		public UserForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();	
			//loadParameters();
			ListBoxShow();	
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			
		}
		
		void ListBoxShow()
		{
			string sql = @" Select UserNM as name, UserPW as password from [LabelPrintDB].[dbo].[UserLogin]";	
			
			DataTable dt = ToolsClass.PullData(sql);
			
			
			foreach (DataRow row in dt.Rows) {
				
				LB_User.Items.AddRange(new object[] {row[0]});
				
			}
			
		}
//		private void loadParameters()
//		{
//			connDB = ToolsClass.getConfig ("Conn",false,"","Config.xml");			
//		}
		
		void Bt_AddClick(object sender, EventArgs e)
		{
			
				NewUserForm frm = new NewUserForm();
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();
		}
		
		void Bt_EditClick(object sender, EventArgs e)
		{
			if(!(LB_User.SelectedIndex > -1))
				return;
			
				EditUserForm frm = new EditUserForm(LB_User.SelectedItem.ToString());
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();
		}
		
		void Bt_DeleteClick(object sender, EventArgs e)
		{
			if(!(LB_User.SelectedIndex > -1))
				return;
			DialogResult dialogResult = MessageBox.Show("Want to Delete User:"+ LB_User.SelectedItem.ToString() +"?","Attention", MessageBoxButtons.YesNo);
			if(dialogResult == DialogResult.Yes)
			{
    			string sql = @"Delete from [LabelPrintDB].[dbo].[UserLogin] where UserNM = '{0}'";
    			sql = string.Format(sql,LB_User.SelectedItem.ToString());
    			int i = ToolsClass.PutsData(sql);
    			MessageBox.Show("Deleting done");
			}
			else if (dialogResult == DialogResult.No)
				return;		
		}
		
		void Bt_ExitClick(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
