﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/10/2016
 * Time: 3:55 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;

namespace LabelPrint
{
	/// <summary>
	/// Description of AddNominalPowerForm.
	/// </summary>
	public partial class AddNominalPowerForm : Form
	{
		public AddNominalPowerForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		void BtCancelClick(object sender, EventArgs e)
		{
			this.Close();
		}
		
		void BtSubmitClick(object sender, EventArgs e)
		{
			if(string.IsNullOrEmpty(TbCode.Text.ToString()))
				return;
			if(string.IsNullOrEmpty(TbName.Text.ToString()))
				return;
			
			string sql = @"select * from [SchemeEntry] where [PmaxID] = '{0}' or [PmaxNM] = '{1}'";
				sql = string.Format(sql,TbCode.Text.ToString(),TbName.Text.ToString());
					DataTable dt = ToolsClass.PullData(sql);
				if (dt.Rows.Count > 0) {
						MessageBox.Show("Record has already exists!");
						return;
				}
				
				EditNominalPowerForm frm = new EditNominalPowerForm(TbCode.Text.ToString(),TbName.Text.ToString());
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();					
				
		}
	}
}
