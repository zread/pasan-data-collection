﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/10/2016
 * Time: 4:08 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace LabelPrint
{
	/// <summary>
	/// Description of EditNominalPowerForm.
	/// </summary>
	public partial class EditNominalPowerForm : Form
	{
		private string  _Code = "";
		private string _Name ="";
		public EditNominalPowerForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			SetNominalPowerRange();
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		

			public EditNominalPowerForm(string Code, string Name)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			SetNominalPowerRange();
				
			TbCode.Enabled = false;
			TbCode.Text = Code;
			_Code = Code;
			TbName.Enabled = false;
			TbName.Text = Name;
			_Name = Name;
			ShowRecord();
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
	
			void SetNominalPowerRange()
			{
				string sql = @"SELECT ItemValue from Item where ItemClassInterID = 2";
				DataTable dt = ToolsClass.PullData(sql);
				if(dt.Rows.Count < 1)
				{
					MessageBox.Show("Please maintain the nominal power range fist!");
					this.Close();
					return;
				}
				foreach ( DataRow row in dt.Rows) {
					
					CbNominalPower.Items.AddRange(new object[]{row[0].ToString()} );
				}
				

			}
			void ShowRecord()
			{
				string sql = @"select  [PmaxNum]
									  ,[PmaxID]
								      ,[PmaxNM]
								      ,[Pmax]
								      ,[MinValue]
								      ,[CtlValue]
								      ,[MaxValue]
								      ,[RESV1]
								      ,[RESV2]
								      ,[RESV3] From SchemeEntry where PmaxID = '{0}' and PmaxNM = '{1}' order by PmaxNum";
					sql = string.Format(sql,_Code,_Name);
				DataTable dt = ToolsClass.PullData(sql);
				foreach ( DataRow  row  in dt.Rows) {
					string[]name = {row[0].ToString(),row[1].ToString(),row[2].ToString(),row[3].ToString(),row[4].ToString(),row[5].ToString(),row[6].ToString(),row[7].ToString(),row[8].ToString(),row[9].ToString()};
					dataGridView1.Rows.Add(name);		
					}
			}
		void BtAddClick(object sender, EventArgs e)
		{
			if(!VarifyData())
			{
				MessageBox.Show("Please fill All info!");
				return;
			}
			if(!VarifyNumberrange())
			{
				MessageBox.Show("Please verify input logic!");
				return;
			}
			
			ListClass ListAdd = AssignNewListClass();
			
			string[]name = {(dataGridView1.Rows.Count + 1).ToString(),ListAdd.PMaxID,ListAdd.PMaxNM,ListAdd.PMax,ListAdd.MinValue,ListAdd.CtlValue,ListAdd.MaxValue,ListAdd.RESV1,ListAdd.RESV2,ListAdd.RESV3};
			//string[]name = {ListAdd.PMaxNum,ListAdd.PMaxID,ListAdd.PMaxNM,ListAdd.PMax,ListAdd.MinValue,ListAdd.CtlValue,ListAdd.MaxValue,ListAdd.RESV1,ListAdd.RESV2,ListAdd.RESV3};
			dataGridView1.Rows.Add(name);
		
		}
		
		private bool VarifyNumberrange()
		{
		
			if(CbOpr1.Text.ToString().Equals("=") || CbOpr2.Text.ToString().Equals("=") )
			{
				if(CbOpr1.Text.ToString() != CbOpr2.Text.ToString())
					return false;
				if(TbValue1.Text.ToString() != TbValue2.Text.ToString())
					return false;			
			}
			else
			{
				if(Convert.ToInt16(TbValue1.Text.ToString()) >Convert.ToInt16(TbValue2.Text.ToString()))
					return false;
			}
			return true;
		}
		private ListClass AssignNewListClass()
		{
			ListClass lst1 = new ListClass();
			lst1.PMaxID = TbCode.Text.ToString();
			lst1.PMaxNM = TbName.Text.ToString();
			lst1.PMax = CbNominalPower.Text.ToString() + "W";
			
			int num = 0;
			string sql = @"select top 1 PmaxNum from SchemeEntry where PmaxID = '{0}' and PmaxNM = '{1}' and Pmax = '{2}' order by PmaxNum desc";
			sql = string.Format(sql,lst1.PMaxID,lst1.PMaxNM,lst1.PMax);
			SqlDataReader rd = ToolsClass.GetDataReader(sql);
			if(rd != null && rd.Read())
			{
				num = Convert.ToInt32(rd.GetValue(0));
				lst1.PMaxNum = num.ToString();
			}
			else
			{
				sql = @"select top 1 PmaxNum from SchemeEntry where PmaxID = '{0}' and PmaxNM = '{1}' order by PmaxNum desc";
				sql = string.Format(sql,lst1.PMaxID,lst1.PMaxNM);
				rd = ToolsClass.GetDataReader(sql);
					if(rd != null && rd.Read())
					{
						num = Convert.ToInt32(rd.GetValue(0));
					    num ++;
					}
			
				lst1.PMaxNum = num.ToString();
			}
			
			lst1.MinValue = "[Measured power]"+ CbOpr1.Text.ToString() +" " +TbValue1.Text.ToString();
			lst1.MaxValue = "[Measured power]"+ CbOpr2.Text.ToString() +" "+TbValue2.Text.ToString();
			lst1.CtlValue = CbCtl.Text.ToString();
			if(!string.IsNullOrEmpty(TbPower1.Text.ToString()))
				lst1.RESV1 = TbPower1.Text.ToString();
			if(!string.IsNullOrEmpty(TbPower2.Text.ToString()))
				lst1.RESV3 = TbPower2.Text.ToString();	
			if(!string.IsNullOrEmpty(CbDisplay.Text.ToString()))
				lst1.RESV2= CbDisplay.Text.ToString();

			return lst1;			
			
		}
		
		private bool VarifyData()
		{
			bool check = true;
			
			foreach (Control ctl in Controls) 
			{
				if(ctl.TabIndex > 99)
					continue;				
				if(string.IsNullOrEmpty(ctl.Text.ToString()))
					check = false;
			}
			return check;
		}
	
		
		void BtQuitClick(object sender, EventArgs e)
		{
			this.Close();
		}
		
		void BtSubmitClick(object sender, EventArgs e)
		{
			foreach (DataGridViewRow row in dataGridView1.Rows) {
				ListClass NC = new ListClass();
				NC.PMaxNum = row.Cells["ID"].Value.ToString();
				NC.PMaxID = row.Cells["PmaxID"].Value.ToString();
				NC.PMaxNM = row.Cells["PmaxNM"].Value.ToString();
				NC.PMax = row.Cells["PMax"].Value.ToString();
				NC.MaxValue = row.Cells["MaxValue"].Value.ToString();
				NC.MinValue = row.Cells["MinValue"].Value.ToString();
				NC.CtlValue = row.Cells["Ctl"].Value.ToString();
				NC.RESV1 = row.Cells["Resv1"].Value.ToString();
				NC.RESV2 = row.Cells["Resv2"].Value.ToString();
				NC.RESV3 = row.Cells["Resv3"].Value.ToString();	
			
				string sql = @"select * from SchemeEntry where PmaxID = '{0}' and PmaxNM = '{1}' and PmaxNum = '{2}'";
				sql = string.Format(sql, NC.PMaxID, NC.PMaxNM, NC.PMaxNum);
				SqlDataReader rd = ToolsClass.GetDataReader(sql);
				if(rd != null && rd.Read())
				{
					sql = @"update schemeentry set Pmax = '{3}', 
							MinValue = '{4}', CtlValue = '{5}', MaxValue ='{6}',
							CreateDate = null,createuser = null,ModifyDate = null,
							ModifyUser =null, Resv1 = '{11}', RESV2 ='{12}',RESV3 ='{13}'
							where PmaxID = '{0}' and PmaxNM ='{1}' and PmaxNum = '{2}'";
					sql = string.Format(sql,NC.PMaxID,NC.PMaxNM,NC.PMaxNum,
					                    NC.PMax,NC.MinValue,NC.CtlValue,
					                    NC.MaxValue,NC.CreateDate,NC.CreateUser,
					                    NC.ModifyDate,NC.ModifyUser,NC.RESV1,NC.RESV2,NC.RESV3);
					ToolsClass.PutsData(sql);
				}
				else
				{
					sql = @"insert into schemeentry values('{0}','{1}','{2}','{3}','{4}','{5}','{6}',null,null,null,null,'{11}','{12}','{13}')";
					sql = string.Format(sql,NC.PMaxID,NC.PMaxNM,NC.PMaxNum,
					                    NC.PMax,NC.MinValue,NC.CtlValue,
					                    NC.MaxValue,NC.CreateDate,NC.CreateUser,
					                    NC.ModifyDate,NC.ModifyUser,NC.RESV1,NC.RESV2,NC.RESV3);
					ToolsClass.PutsData(sql);
				}
				
			}
			this.Close();
		}
		
		void BtEditClick(object sender, EventArgs e)
		{
			if(dataGridView1.CurrentRow.Index < 0)
				return;
			int rowindex = dataGridView1.CurrentRow.Index;
			if(!VarifyData())
				return;
			if(!VarifyNumberrange())
				return;
			ListClass ListAdd = AssignNewListClass();			
			string[]name = {ListAdd.PMaxNum,ListAdd.PMaxID,ListAdd.PMaxNM,ListAdd.PMax,ListAdd.MinValue,ListAdd.CtlValue,ListAdd.MaxValue,ListAdd.RESV1,ListAdd.RESV2,ListAdd.RESV3};
			dataGridView1.Rows[rowindex].SetValues(name);
			dataGridView1.Rows.Clear();
			ShowRecord();
		}
		
		
		void DataGridView1CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			
			if(dataGridView1.CurrentRow.Index < 0)
				return;
			int rowindex = dataGridView1.CurrentRow.Index;				
			
				string max = dataGridView1.Rows[rowindex].Cells["MaxValue"].Value.ToString();
				string min = dataGridView1.Rows[rowindex].Cells["MinValue"].Value.ToString();
			
				if(max.Substring(16,2) == "<=")
				{
					CbOpr2.Text = "<=";
					TbValue2.Text = max.Substring(18, max.Length- 18);
				}
				else
				{
					CbOpr2.Text = max.Substring(16,1);
					TbValue2.Text = max.Substring(17, max.Length- 17);
				}
				
				if(min.Substring(16,2) == ">=")
				{
					CbOpr1.Text = ">=";
					TbValue1.Text = min.Substring(18, min.Length- 18);
				}
				else
				{
					CbOpr1.Text = min.Substring(16,1);
					TbValue1.Text = min.Substring(17, min.Length- 17);
				}
								
				CbNominalPower.Text = dataGridView1.Rows[rowindex].Cells["PMax"].Value.ToString();
				CbCtl.Text = dataGridView1.Rows[rowindex].Cells["Ctl"].Value.ToString();
				TbPower1.Text = dataGridView1.Rows[rowindex].Cells["Resv1"].Value.ToString();
				CbDisplay.Text = dataGridView1.Rows[rowindex].Cells["Resv2"].Value.ToString();
				TbPower2.Text = dataGridView1.Rows[rowindex].Cells["Resv3"].Value.ToString();	
		}
		
		void BtDeleteClick(object sender, EventArgs e)
		{
			if(dataGridView1.CurrentRow.Index < 0)
				return;
			int rowindex = dataGridView1.CurrentRow.Index;	
			
				dataGridView1.Rows[rowindex].Cells["PmaxNM"].Value.ToString();
			
			string sql= @"delete from schemeentry where PmaxID = '{0}' and PmaxNM ='{1}' and PmaxNum = '{2}'";
			sql = string.Format(sql,_Code,_Name,dataGridView1.Rows[rowindex].Cells["ID"].Value.ToString());
			ToolsClass.PutsData(sql);
			dataGridView1.Rows.Clear();
			ShowRecord();
		}
}
}
